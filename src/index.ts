#!/usr/bin/env node
import glob from 'tiny-glob/sync';
import {
  rollup,
  RollupOptions,
  OutputOptions,
} from 'rollup';
import asyncro from 'asyncro';
import * as fs from 'fs-extra';
import logError from './logError';
import { paths } from './constants';
import { createBuildConfigs } from './createBuildConfigs';
import {
  resolveApp,
} from './utils';
import { concatAllArray } from 'jpjs';
import {
  PackageJson,
  WatchOpts,
  BuildOpts,
  ModuleFormat,
  NormalizedOpts,
} from './types';
import { createProgressEstimator } from './createProgressEstimator';
import * as deprecated from './deprecated';

let appPackageJson: PackageJson;

try {
  appPackageJson = fs.readJSONSync(paths.appPackageJson);
} catch (e) {}

export const isDir = (name: string) =>
  fs
    .stat(name)
    .then(stats => stats.isDirectory())
    .catch(() => false);

export const isFile = (name: string) =>
  fs
    .stat(name)
    .then(stats => stats.isFile())
    .catch(() => false);

async function jsOrTs(filename: string) {
  const extension = (await isFile(resolveApp(filename + '.ts')))
    ? '.ts'
    : (await isFile(resolveApp(filename + '.tsx')))
    ? '.tsx'
    : (await isFile(resolveApp(filename + '.jsx')))
    ? '.jsx'
    : '.js';

  return resolveApp(`${filename}${extension}`);
}

async function getInputs(
  entries?: string | string[],
  source?: string
): Promise<string[]> {
  return concatAllArray(
    ([] as any[])
      .concat(
        entries && entries.length
          ? entries
          : (source && resolveApp(source)) ||
              ((await isDir(resolveApp('src'))) && (await jsOrTs('src/index')))
      )
      .map(file => glob(file))
  );
}

async function normalizeOpts(opts: WatchOpts): Promise<NormalizedOpts> {
  return {
    ...opts,
    name: opts.name || appPackageJson.name,
    input: await getInputs(opts.entry, appPackageJson.source),
    format: opts.format.split(',').map((format: string) => {
      if (format === 'es') {
        return 'esm';
      }
      return format;
    }) as [ModuleFormat, ...ModuleFormat[]],
  };
}

async function cleanDistFolder(dist: string) {
  await fs.remove(dist);
}

async function buildProject(dirtyOpts: BuildOpts) {
  const opts = await normalizeOpts(dirtyOpts);
  const buildConfigs = await createBuildConfigs(opts);
  await cleanDistFolder(opts.dist);
  const logger = await createProgressEstimator();
  try {
    const promise = asyncro
        .map(
            buildConfigs,
            async (inputOptions: RollupOptions & { output: OutputOptions }) => {
              let bundle = await rollup(inputOptions);
              await bundle.write(inputOptions.output);
            }
        )
        .catch((e: any) => {
          throw e;
        })
        .then(async () => {
          await deprecated.moveTypes(opts.dist);
        });
    logger(promise, 'Building modules');
    await promise;
  } catch (error) {
    logError(error);
    process.exit(1);
  }
}


export default {
  build: buildProject,
}